package com.company.contoller;

import com.company.Commands;

import com.company.exception.ObjectIsNotFound;
import com.company.service.ProjectService;
import com.company.service.TaskService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class Controller {

    private TaskService taskService;
    private ProjectService projectService;

    public void execute (){
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        boolean loop = true;
        while (loop) {
            Scanner sc = new Scanner(System.in);
            String line = sc.nextLine();
            Commands commands = Commands.help;

            for (Commands com : Commands.values()) {
                if (line.equalsIgnoreCase(com.getName())) {
                    commands = com;
                    break;
                }
            }
            switch (commands) {
                case projectcreate:
                    projectCreate();
                    break;

                case projectlist:
                    projectService.projectList();
                    break;

                case projectclear:
                    projectService.projectClear();
                    break;

                case projectremove:
                    projectRemove();
                    break;


                case taskcreate:
                    taskCreate();
                    break;


                case tasklist:
                   taskService.taskList();
                    break;

                case taskclear:
                    taskService.taskClear();
                    break;

                case taskremove:
                 taskRemove();
                 break;


                    case help:
                        help();
                    break;


                case exit:
                    loop = false;
                    break;



            }
        }
    }

    public void projectCreate(){
        Scanner sc = new Scanner(System.in);
        System.out.println("ENTER NAME:");
        String name = sc.nextLine().trim();
        if (name.isEmpty()) {
            System.out.println("EMPTY NAME");
            return;
        }
        System.out.println("ENTER THE TIME OF THE BEGINNING OF THE PROJECT");
        String dateStart = sc.nextLine().trim();
        System.out.println("ENTER THE END TIME OF THE PROJECT");
        String dateEnd = sc.nextLine().trim();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
        try {
            System.out.println("YOUR PROJECT ID IS");
            System.out.println(projectService.projectCreate(name, dateFormatter.parse(dateStart),dateFormatter.parse(dateEnd)));
        } catch (Exception e) {
            System.out.println("Wrong format dd.mm.yyyy");

        }

    }

    public void projectRemove(){
        System.out.println("ENTER ID");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        try {
            projectService.projectRemove(line);
        } catch (ObjectIsNotFound objectIsNotFound) {
            objectIsNotFound.printStackTrace();
        }
        System.out.println("PROJECT&TASK REMOVED");

    }

    public void taskCreate(){
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine().trim();
        if(line.isEmpty()){
            System.out.println("EMPTY NAME");
            return;
        }
        System.out.println("ENTER PROJECT ID:");
        String projectID = sc.nextLine().trim();
        System.out.println("ENTER THE TIME OF THE BEGINNING OF THE TASK");
        String dateStart = sc.nextLine().trim();
        System.out.println("ENTER THE END TIME OF THE TASK");
        String dateEnd = sc.nextLine().trim();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
        try{
            System.out.println("TASK ID IS");
            System.out.println(taskService.taskCreate(line,projectID,dateFormatter.parse(dateStart),dateFormatter.parse(dateEnd)));
        }
        catch (ParseException e){
            System.out.println("Wrong format dd.mm.yyyy");
        }
        catch (ObjectIsNotFound e){
            System.out.println("Такого проекта не сущесвует");
        }
    }
    public void taskRemove(){
        System.out.println("ENTER ID");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        try {
            taskService.taskRemove(line);
        } catch (ObjectIsNotFound objectIsNotFound) {
            objectIsNotFound.printStackTrace();
            return;
        }
        System.out.println("TASK REMOVED");

    }

    public void help(){
        Commands[] commands2 = Commands.values();
        for (Commands cm : commands2) {
            System.out.println(cm);
        }

    }


    public TaskService getTaskService() {
        return taskService;
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }


    public ProjectService getProjectService() {
        return projectService;
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }


}
