package com.company;

import com.company.entity.Project;
import com.company.entity.Task;

import java.text.SimpleDateFormat;
import java.util.*;

public class Main {
    static Map<String, Project> projectMap = new HashMap<>();
    static Map<String, Task> taskMap = new HashMap<>();


    public static void main(String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");

        boolean loop = true;

        while (loop) {
            Scanner sc = new Scanner(System.in);
            String line = sc.nextLine();
            Commands commands = Commands.help;

            for (Commands com : Commands.values()) {
                if (line.equalsIgnoreCase(com.getName())) {
                    commands = com;
                    break;
                }
            }
            switch (commands) {
                case projectcreate:
                    projectCreate();
                    break;

                case projectlist:
                    projectList();
                    break;

                case projectclear:
                    projectClear();
                    break;

                case projectremove:
                    projectRemove();
                    break;


                case taskcreate:
                    taskCreate();
                    break;


                case tasklist:
                    taskList();
                    break;

                case taskclear:
                    taskClear();
                    break;

                case taskremove:
                    taskRemove();
                    break;


                case help:
                    help();
                    break;

                case exit:
                    loop = false;
                    break;

            }
        }
    }

    public static void projectCreate() {
        String line;
        Scanner sc = new Scanner(System.in);
        System.out.println("[PROJECT CREATE] test");
        System.out.println("ENTER NAME:");
        line = sc.nextLine().trim();
        if (line.isEmpty()) {
            System.out.println("EMPTY NAME");
            return;
        }
        System.out.println("YOUR PROJECT ID IS:");
        String id2 = UUID.randomUUID().toString();
        System.out.println(id2);
        Project project = new Project(line, id2);
        System.out.println("ENTER THE TIME OF THE BEGINNING OF THE PROJECT");
        line = sc.nextLine().trim();
        System.out.println("ENTER THE END TIME OF THE PROJECT");
        String dateOfTheEnd = sc.nextLine().trim();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
        try {
            project.setDateBegin(dateFormatter.parse(line));
            project.setDateEnd(dateFormatter.parse(dateOfTheEnd));
        } catch (Exception e) {
            System.out.println("Wrong format dd.mm.yyyy");
            return;
        }
        projectMap.put(id2, project);
        System.out.println("[OK]");
    }

    public static void projectList() {
        System.out.println("[PROJECT LIST]");
        for (Map.Entry<String, Project> projectEntry : projectMap.entrySet()) {
            System.out.printf("ID: %s  Name: %s \n", projectEntry.getKey(), projectEntry.getValue().getName());
        }
    }

    public static void projectClear() {
        projectMap.clear();
        taskMap.clear();
        System.out.println("[ALL PROJECTS REMOVED]");
    }

    public static void projectRemove() {
        Scanner sc = new Scanner(System.in);
        System.out.println("ENTER ID:");
        String id = sc.nextLine();
        projectMap.remove(id);
        Iterator<Map.Entry<String, Task>> iterator = taskMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Task> entry = iterator.next();
            if (id.equals(entry.getValue().getProjectID())) {
                iterator.remove();
                System.out.println("Задача " + entry.getValue().getName() + " удалена вместе с проектом");
            }
        }
        System.out.println("PROJECT REMOVED");
    }


    public static void taskCreate() {
        String line;
        Scanner sc = new Scanner(System.in);
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        line = sc.nextLine().trim();
        if (line.isEmpty()) {
            System.out.println("YOU ARE AN IDIOT");
            return;
        }
        System.out.println("ENTER PROJECT ID:");
        String projectId = sc.nextLine().trim();
        System.out.println("YOUR TASK ID IS:");
        String id2 = UUID.randomUUID().toString();
        System.out.println(id2);
        Task task = new Task(line, id2);
        task.setProjectID(projectId);
        System.out.println("ENTER THE TIME OF THE BEGINNING OF THE TASK");
        line = sc.nextLine().trim();
        System.out.println("ENTER THE END TIME OF THE TASK");
        String dateOfTheEnd = sc.nextLine().trim();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
        try {
            task.setDateBegin(dateFormatter.parse(line));
            task.setDateEnd(dateFormatter.parse(dateOfTheEnd));
        } catch (Exception e) {
            System.out.println("Wrong format dd.mm.yyyy");
            return;
        }
        taskMap.put(id2, task);
        System.out.println("[OK]");
    }

    public static void taskList() {
        System.out.println("[TASK LIST]");
        for (Map.Entry<String, Task> taskEntry : taskMap.entrySet()) {
            System.out.printf("ID: %s  TaskName: %s \n", taskEntry.getKey(), taskEntry.getValue().getName());
        }
    }

    public static void taskRemove() {
        Scanner sc = new Scanner(System.in);
        System.out.println("ENTER ID:");
        String id = sc.nextLine();
        taskMap.remove(id);
        System.out.println("TASK REMOVED");
    }

    public static void taskClear() {
        taskMap.clear();
        System.out.println("[ALL TASKS REMOVED]");
    }

    public static void help() {
        Commands[] commands2 = Commands.values();
        for (Commands cm : commands2) {
            System.out.println(cm);
        }
    }

    public static void projectTasks() {
        System.out.println("ENTER PROJECT ID");
        Scanner sc = new Scanner(System.in);
        String id = sc.nextLine().trim();
        for (Map.Entry<String, Task> taskEntry : taskMap.entrySet()) {
            if (taskEntry.getValue().getProjectID().equals(id)) {
                System.out.println("Задача с айди проекта " + id + " имя таски " + taskEntry.getValue().getName());
            }
        }
    }


}

