package com.company.repository;

import com.company.entity.Project;
import com.company.exception.ObjectIsAlreadyExist;

import java.util.*;

public class ProjectRepository {

    public Map<String, Project> projectRepository;
    private TaskRepository taskRepository;


    public Project findOne (String id){
        return projectRepository.get(id);
    }

    public void findAll(){
        for (Map.Entry<String, Project> projectEntry : projectRepository.entrySet()) {
            System.out.printf("ID: %s  Name: %s \n", projectEntry.getKey(), projectEntry.getValue().getName());
        }
    }

    public void persist(Project project) throws Exception // При персисте, ты по ид достаёшь из мапы, если пусто сохраняешь то, что прислал сервис
    {
        Project projectPersist = projectRepository.get(project.getId());
        if (projectPersist ==null){
            projectRepository.put(project.getId(), project);
        }
        else throw new ObjectIsAlreadyExist();
    }

    public void merge (Project project) // при мерже ты просто толкаешь в мапу
    {
       projectRepository.put(project.getId(),project);
    }

    public void remove(String id){
        projectRepository.remove(id);


        /*
        Iterator<Map.Entry<String, Task>> iterator = taskRepository.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Task> entry = iterator.next();
            if (id.equals(entry.getValue().getProjectID())) {
                iterator.remove();
            }
        }

         */
    }

    public void removeAll(){
        projectRepository.clear();
        taskRepository.removeAll();
    }

    public Project getFromProjectRepository(String name){
        return projectRepository.get(name);
    }

    public void setProjectRepositoryMap(Map<String, Project> projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

}
