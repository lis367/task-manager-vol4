package com.company.repository;

import com.company.entity.Task;
import com.company.exception.ObjectIsAlreadyExist;

import java.util.ArrayList;
import java.util.Map;

public class TaskRepository {

    private Map<String, Task> taskRepository;

    public void persist(Task task) throws Exception {
        Task taskPersist = taskRepository.get(task.getId());
        if (taskPersist ==null){
            taskRepository.put(task.getId(), task);
        }
        else throw new ObjectIsAlreadyExist();
    }

    public Task findOne(String id){
        return taskRepository.get(id);
    }

    public void findAll(){
        for (Map.Entry<String, Task> taskEntry : taskRepository.entrySet()) {
            System.out.printf("ID: %s  Name: %s \n", taskEntry.getKey(), taskEntry.getValue().getName());
        }
    }

    public void merge (Task task)
    {
        taskRepository.put(task.getId(), task);
    }

    public void remove(String id){
        taskRepository.remove(id);
    }

    public void removeAll(){
        taskRepository.clear();
    }

    public ArrayList getTasksFromProject(String projectID){
        ArrayList<String> arrayList = new ArrayList<String>();
        for (Map.Entry<String, Task> taskEntry : taskRepository.entrySet()) {
           if(projectID.equals(taskEntry.getValue().getProjectID())){
               arrayList.add(taskEntry.getValue().getId());
            }
        }
        return arrayList;
    }

    public void setTaskRepositoryMap(Map<String, Task> taskRepository) {
        this.taskRepository = taskRepository;
    }


}

