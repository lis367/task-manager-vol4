package com.company;

import com.company.util.Bootstrap;

public class Application {

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}
