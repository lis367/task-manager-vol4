package com.company.util;

import com.company.contoller.Controller;
import com.company.entity.Project;
import com.company.entity.Task;
import com.company.repository.ProjectRepository;
import com.company.repository.TaskRepository;
import com.company.service.ProjectService;
import com.company.service.TaskService;

import java.util.HashMap;
import java.util.Map;

public class Bootstrap {

    public void init(){
        ProjectService projectService = new ProjectService();
        TaskService taskService = new TaskService();
        ProjectRepository projectRepository = new ProjectRepository();
        Map<String, Project> projectRepositoryMap = new HashMap<>();
        projectRepository.setProjectRepositoryMap(projectRepositoryMap);
        TaskRepository taskRepository = new TaskRepository();
        Map<String, Task> taskRepositoryMap = new HashMap<>();
        taskRepository.setTaskRepositoryMap(taskRepositoryMap);
        projectRepository.setTaskRepository(taskRepository);
        projectService.setProjectRepository(projectRepository);
        projectService.setTaskRepository(taskRepository);
        taskService.setTaskRepository(taskRepository);
        taskService.setProjectService(projectService);

        Controller controller = new Controller();
        controller.setProjectService(projectService);
        controller.setTaskService(taskService);
        controller.execute();

    }
}
