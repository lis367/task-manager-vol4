package com.company.service;

import com.company.entity.Task;
import com.company.exception.ObjectIsNotFound;
import com.company.repository.TaskRepository;

import java.util.Date;
import java.util.UUID;

public class TaskService {

    private TaskRepository taskRepository;
    private ProjectService projectService;



    public String taskCreate(String name, String projectId, Date dateStart, Date dateEnd) throws ObjectIsNotFound {
        if (projectService.findProject(projectId)==null){
           throw new ObjectIsNotFound();
        }
        String id = UUID.randomUUID().toString();
        Task task = new Task(name, id);
        task.setDateBegin(dateStart);
        task.setDateEnd(dateEnd);
        task.setProjectID(projectId);
        taskRepository.merge(task);
        return task.getId();
    }

    public void taskList() {
        taskRepository.findAll();
    }

    public void taskClear() {
        taskRepository.removeAll();
    }

    public void taskRemove(String id) throws ObjectIsNotFound {
        if(taskRepository.findOne(id)==null){
            throw new ObjectIsNotFound();
        }
        else {
            taskRepository.remove(id);
        }
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

}
