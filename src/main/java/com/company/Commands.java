package com.company;

public enum Commands {
    projectcreate("project-create","Create new project."),
    projectlist("project-list","Show all projects."),
    projectclear("project-clear","Remoce all projects"),
    projectremove("project-remove","Remove selected project."),
    taskcreate("task-create","Create new task"),
    tasklist("task-list","Show all tasks"),
    taskclear("task-clear","Remove all tasks"),
    taskremove("task-remove","Remove selected task"),
    help("help","Show all commands."),
    exit("Exit","exit"),
    ;
    private String name;
    private String description;

    public String getName() {
        return name;
    }
    Commands(String name, String description) {
        this.name = name;
        this.description = description;
    }
    public String toString(){
        return name +" "+ description;
    }
}
